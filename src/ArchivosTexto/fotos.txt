#---------------------------------------------------------
# Fotos_Persona
# 
#---------------------------------------------------------

total.fotos = 7

foto1.id= 000
foto1.idPersona = 000
foto1.url= data/compu.jpg
foto1.fecha = 2019-05-01
foto1.lugar = Guayaquil
foto1.idAlbum = 000
foto1.idReaccion = 000,001,002
foto1.idComentario = 000,001
foto1.Descripcion = Foto de prueba 1
foto1.idHashtags = 000,001
foto1.idPersonas = 000,001
foto1.camara= camara prueba 1
    
foto2.id= 001
foto2.idPersona = 000
foto2.url= data/photo1.jpg
foto2.fecha = 2018-03-01
foto2.lugar = Quito
foto2.idAlbum = 000
foto2.idReaccion = 001
foto2.idComentario = 002
foto2.Descripcion = Foto de prueba 2
foto2.idHashtags = 001
foto2.idPersonas = 001
foto2.camara= camara prueba 2
    
foto3.id= 003
foto3.idPersona = 001
foto3.url= data/photo2.jpg
foto3.fecha = 2019-06-30
foto3.lugar = Quito
foto3.idAlbum = 002
foto3.idReaccion = 001
foto3.idComentario = 002
foto3.Descripcion = Foto de prueba usuario 2
foto3.idHashtags = 003
foto3.idPersonas = 001
foto3.camara= camara prueba 1 usario 2
    
foto4.id= 004
foto4.idPersona = 000
foto4.url= data/img1.jpg
foto4.fecha = 2017-06-30
foto4.lugar = Guayaquil
foto4.idAlbum = 001
foto4.idReaccion = 000,001,002
foto4.idComentario = 000
foto4.Descripcion = Foto de prueba 1
foto4.idHashtags = 000,001
foto4.idPersonas = 000,001
foto4.camara= camara prueba 1
    
foto5.id= 005
foto5.idPersona = 000
foto5.url= data/logo.jpg
foto5.fecha = 2016-05-30
foto5.lugar = Quito
foto5.idAlbum = 000,001
foto5.idReaccion = 000,001
foto5.idComentario = 002
foto5.Descripcion = Foto de prueba 2
foto5.idHashtags = 003
foto5.idPersonas = 001
foto5.camara= camara prueba 2
    
foto6.id= 006
foto6.idPersona = 001
foto6.url= data/logo.jpg
foto6.fecha = 2019-06-28
foto6.lugar = Quito
foto6.idAlbum = 002
foto6.idReaccion = 001
foto6.idComentario = 002
foto6.Descripcion = Foto de prueba usuario 2
foto6.idHashtags = 001
foto6.idPersonas = 001
foto6.camara= camara prueba 1 usario 2

foto7.id= 007
foto7.idPersona = 000
foto7.url= data/FONDO.jpg
foto7.fecha =2019-05-01
foto7.lugar = Quito
foto7.idAlbum = 000
foto7.idReaccion = 001
foto7.idComentario = 002
foto7.Descripcion = Foto de prueba 2
foto7.idHashtags = 001
foto7.idPersonas = 001
foto7.camara= camara prueba 2
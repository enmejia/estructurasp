/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import TDAs.CircularDoblyList;
import TDAs.LinkedList;

/**
 *
 * @author Evelyn
 */
public class Album {
    private String idAlbum;
    private String nombreAlbum;
    private LinkedList<String> fotos;
    private int nElementos;
    private String comentarios;
    private String idPersona;

    public Album(String idAlbum, String nombreAlbum, LinkedList<String> fotos,int nElementos, String comentarios, String idPersona) {
        this.idAlbum = idAlbum;
        this.nombreAlbum = nombreAlbum;
        this.fotos=fotos;
        this.nElementos = nElementos;
        this.comentarios = comentarios;
        this.idPersona = idPersona;
    }
     public Album( String nombreAlbum, LinkedList<String> fotos,int nElementos, String comentarios, String idPersona) {
                this.nombreAlbum = nombreAlbum;
        this.fotos=fotos;
        this.nElementos = nElementos;
        this.comentarios = comentarios;
        this.idPersona = idPersona;
     }
    public Album() {
        
        
    }

    public String getIdAlbum() {
        return idAlbum;
    }

    public void setIdAlbum(String idAlbum) {
        this.idAlbum = idAlbum;
    }

    public String getNombreAlbum() {
        return nombreAlbum;
    }

    public void setNombreAlbum(String nombreAlbum) {
        this.nombreAlbum = nombreAlbum;
    }

    public LinkedList<String> getFotos() {
        return fotos;
    }

    public void setFotos(LinkedList<String> fotos) {
        this.fotos = fotos;
    }

    public int getnElementos() {
        return nElementos;
    }

    public void setnElementos(int nElementos) {
        this.nElementos = nElementos;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(String idPersona) {
        this.idPersona = idPersona;
    }
    
    
    
}

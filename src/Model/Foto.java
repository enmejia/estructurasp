/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import TDAs.ArrayList;
import TDAs.LinkedList;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Evelyn
 */
public class Foto implements Comparable<Foto> {

    private String URL;
    private String id;

    private String nombreP;
    private Date fecha;
    private String lugar;
    private LinkedList<String> nombreA;
    private ArrayList<String> reacciones;
    private ArrayList<String> comentarios;
    private String descripcion;
    private ArrayList<String> hashtags;
    private ArrayList<String> personas;
    private String camara;

    
    public Foto(String URL, String id,String nombreP, Date fecha, String lugar, LinkedList<String> nombreA, ArrayList<String> reacciones, ArrayList<String> comentarios, String descripcion, ArrayList<String> hashtags, ArrayList<String> personas, String camara) {
        this.URL = URL;
        this.id = id;
        
        this.nombreP = nombreP;
        this.fecha = fecha;
        this.lugar = lugar;
        this.nombreA = nombreA;
        this.reacciones = reacciones;
        this.comentarios = comentarios;
        this.descripcion = descripcion;
        this.hashtags = hashtags;
        this.personas = personas;
        this.camara = camara;
    }
    public Foto(String URL, String id, Date fecha, String lugar, LinkedList<String> nombreA, ArrayList<String> reacciones, ArrayList<String> comentarios, String descripcion, ArrayList<String> hashtags, ArrayList<String> personas, String camara) {
        this.URL = URL;
        this.id = id;
        this.fecha = fecha;
        this.lugar = lugar;
        this.nombreA = nombreA;
        this.reacciones = reacciones;
        this.comentarios = comentarios;
        this.descripcion = descripcion;
        this.hashtags = hashtags;
        this.personas = personas;
        this.camara = camara;
    }
    public Foto(String id,String nombreP){
        this.id = id;
        
        this.nombreP = nombreP;
    }
    @Override
    public String toString() {
        return "Foto{" + ", id=" + id + ", nombreP=" + nombreP + ", fecha=" + fecha +  '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.URL);
        hash = 61 * hash + Objects.hashCode(this.id);
        hash = 61 * hash + Objects.hashCode(this.nombreP);
        hash = 61 * hash + Objects.hashCode(this.fecha);
        hash = 61 * hash + Objects.hashCode(this.lugar);
        hash = 61 * hash + Objects.hashCode(this.nombreA);
        hash = 61 * hash + Objects.hashCode(this.reacciones);
        hash = 61 * hash + Objects.hashCode(this.comentarios);
        hash = 61 * hash + Objects.hashCode(this.descripcion);
        hash = 61 * hash + Objects.hashCode(this.hashtags);
        hash = 61 * hash + Objects.hashCode(this.personas);
        hash = 61 * hash + Objects.hashCode(this.camara);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Foto other = (Foto) obj;
        return true;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public LinkedList<String> getNombreA() {
        return nombreA;
    }

    public void setNombreA(LinkedList<String> nombreA) {
        this.nombreA = nombreA;
    }

    public ArrayList<String> getReacciones() {
        return reacciones;
    }

    public void setReacciones(ArrayList<String> reacciones) {
        this.reacciones = reacciones;
    }

    public ArrayList<String> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<String> comentarios) {
        this.comentarios = comentarios;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<String> getHashtags() {
        return hashtags;
    }

    public void setHashtags(ArrayList<String> hashtags) {
        this.hashtags = hashtags;
    }

    public ArrayList<String> getPersonas() {
        return personas;
    }

    public void setPersonas(ArrayList<String> personas) {
        this.personas = personas;
    }

    public String getCamara() {
        return camara;
    }

    public void setCamara(String camara) {
        this.camara = camara;
    }

    
    
    public Foto(){
        
    }

    @Override
    public int compareTo(Foto o2) {
              /*  if(this.fecha.after(o2.fecha)) return 1;
                else if(this.fecha.before(o2.fecha)) return 1;
                else return 0;*/
              return this.getFecha().compareTo(o2.getFecha());
    }
}

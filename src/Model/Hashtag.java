/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Evelyn
 */
public class Hashtag {
    private String id;
    private String contenido;
    private String idP;

    public Hashtag(String id, String contenido, String idP) {
        this.id = id;
        this.contenido = contenido;
        this.id = idP;
    }
    public Hashtag( String contenido,String idP) {
  
        this.contenido = contenido;
        this.idP=idP;
    }

    public String getIdP() {
        return idP;
    }

    public void setIdP(String idP) {
        this.idP = idP;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
    
}

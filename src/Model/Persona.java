/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author Evelyn
 */
public class Persona {
     private String iD;
     private String nombreP;
     private String apellido;
     private String imagenP;
     private String correo; String contraseña;

    public Persona(String nombreP, String apellido, String imagenP, String correo, String contraseña) {
        this.nombreP = nombreP;
        this.apellido = apellido;
        this.imagenP = imagenP;
        this.correo = correo;
        this.contraseña = contraseña;
    }

    public Persona(String iD) {
        this.iD = iD;
    }

    public Persona() {
    }

    public String getID() {
        return iD;
    }

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getImagenP() {
        return imagenP;
    }

    public void setImagenP(String imagenP) {
        this.imagenP = imagenP;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Persona(String iD, String nombreP, String apellido, String imagenP, String correo, String contraseña) {
        this.iD = iD;
        this.nombreP = nombreP;
        this.apellido = apellido;
        this.imagenP = imagenP;
        this.correo = correo;
        this.contraseña = contraseña;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.iD);
        hash = 41 * hash + Objects.hashCode(this.nombreP);
        hash = 41 * hash + Objects.hashCode(this.apellido);
        hash = 41 * hash + Objects.hashCode(this.imagenP);
        hash = 41 * hash + Objects.hashCode(this.correo);
        hash = 41 * hash + Objects.hashCode(this.contraseña);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        return true;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombreP=" + nombreP + ", apellido=" + apellido + ", imagenP=" + imagenP + ", correo=" + correo + ", contrase\u00f1a=" + contraseña + '}';
    }
     
             
}

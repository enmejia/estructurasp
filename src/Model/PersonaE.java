/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.HashMap;

/**
 *
 * @author Evelyn
 */
public class PersonaE {
    private String id,nombre,apellido,idPersona;
    private HashMap<String,Persona> mapPersonaE;

    public PersonaE(String id, String nombre, String apellido, String idPersona) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.idPersona = idPersona;
    }
    public PersonaE(String nombre, String apellido, String idPersona) {
        
        this.nombre = nombre;
        this.apellido = apellido;
        this.idPersona = idPersona;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(String idPersona) {
        this.idPersona = idPersona;
    }

    public HashMap<String, Persona> getMapPersonaE() {
        return mapPersonaE;
    }

    public void setMapPersonaE(HashMap<String, Persona> mapPersonaE) {
        this.mapPersonaE = mapPersonaE;
    }
    

}

package TDAs;
import List.List;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 *
 * @author Jocellyn Luna
 * @param <E>
 */
public class ArrayList<E> implements List<E>,Iterable<E>  {
    private E[] array;
    private int capacity = 10;
    private int efectivo;
    
    private int j = 0; 
    private boolean removable = false;

    public ArrayList(){ //Todas las TDA van a inicializar vacias
        //No se puede hacer new de E directamente, por ello hay que hacer casting 
        array = (E[]) new Object[capacity];    
        efectivo = 0;
    }
    
    @Override
    public boolean addFirst(E element) {
        if(element == null){
            return false;
        }else if(isEmpty()){
            array[efectivo ++] = element;
            return true;
        }else if (capacity == efectivo){
            addCapacity();
        }
        
        for(int i = efectivo -1 ; i >= 0; i-- ){
            array[i+1] = array[i];
        }
        
        
        array [0] = element;
        efectivo++;
        return true;   
    }
    
    @Override
    public boolean insert (int  index, E element){
        if(element == null || index >= efectivo)
            return false;
        else if (efectivo == capacity)
            addCapacity();
        
        for(int i = efectivo; i > index; i--){
            array[i] = array[i-1];
        }
        
        array[index] = element;
        efectivo++;
        return true;
    }
    
    @Override
    public boolean addLast(E element) {
        if(element == null){
            return false;
        }else if(efectivo == capacity){
            addCapacity();
        }
        
        array[efectivo++] = element;
        return true;
    }
    
    private void addCapacity(){
        E[] tmp = (E[]) new Object [capacity*2];//aumenta su capacidad 50%
        for(int i = 0; i < capacity; i++){
            tmp[i] = array[i];
        }
        array = tmp;
        capacity = capacity*2;
    }

    @Override
    public E getFirst() {
        return array[0];
    }

    @Override
    public E getLast() {
        return array[efectivo -1];
    }

    @Override
    public boolean removeFirst() {
        if(isEmpty())
            return false;
        efectivo--;
        for(int i = 0; i < efectivo; i++){
            array[i] = array[i+1];
        }
        
        array[efectivo] = null;
        return true;
    }

    @Override
    public boolean removeLast() {
        if(isEmpty()){
            return false;
        }
        
        array[--efectivo] = null;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return efectivo == 0;
    }

    @Override
    public boolean contains(E element) {
        if(element == null || isEmpty()){
            return false;
        }
        
        for(int i = 0; i < efectivo ; i++){
            if(array[i].equals(element)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("[");
        
        for(int i = 0; i < efectivo; i++){
            if(i != efectivo-1){
                s.append(array[i] + ", ");
            }else
               s.append(array[i]);
        }
        
        s.append("]");
        return s.toString();
    }
    
    public List<E> slicing(int start, int end){
        List<E> lista = new ArrayList<>();
        if(start >= end || end > efectivo) {
            return lista;
        }
        
        for(int i = start; i < end; i++ ){
            lista.addLast(array[i]);
        }
        
        return lista;
    }
    
    public boolean add(E e) {
            if(efectivo == capacity){
            addCapacity();
        }
    
        array[efectivo++] = e;
        return true;
    }

    
    public void add(int index, E element) {
        if(efectivo == capacity){
            addCapacity();
        }
        array[index] = element;
        efectivo++;
    }
    
    
    @Override
    public boolean equals(Object o){
        if(o == null || ! (o instanceof ArrayList))
            return false;
        
        ArrayList<E> other = (ArrayList<E>) o;
        
        if(this.efectivo != other.efectivo)
            return false;
        
        for(int i =0; i< efectivo ;i++){
            if(!this.array[i].equals(other.array[i]))
                return false;
        }
        
        return true;
    }

    @Override
    public E get(int index) {
        if(efectivo == 0 || index < 0 || index >= efectivo){
            return null;
        }
        return array[index];
    }

    @Override
    public int indexOf(E element) {
        if (element == null){
            return -1;
        }
        
        for(int i = 0; i < efectivo; i++){
            if(array[i].equals(element)){
                return i;
            }
        }
        
        return -1;
    }

    @Override
    public E remove(int index) {
        if(efectivo == 0 || index < 0 || index >= efectivo){
            return null;
        }
        E element = array[index];
        for(int i = index; i < efectivo-1; i++){
            array[index] = array[index +1];
        }
        array[efectivo-1] = null;
        efectivo --;
        return element;
    }

    @Override
    public boolean remove(E element) {
        if(element == null) {
            return false;
        }
        
        for(int i = 0; i < efectivo; i++){
            if(array[i].equals(element)){
                remove(i);
                return true;
            }
        }
        
        return false;
    }

    @Override
    public E set(int index, E element) {
        if (element == null || index < 0 || index >= efectivo){
            return null;
        }
        
        E i = array[index];
        array[index] = element;
        return i;
    }

    @Override
    public int size() {
        return efectivo;
    }
    
   
    


  
   public Iterator<E> iterator( ) { 
       return new MyIterator(); // create a new instance of the inner class
   }

    
    @Override
    public ArrayList<E> findAll(Comparator<E> cmp, E element){
    ArrayList<E> resultado= new ArrayList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==0){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }
    
   
    

    @Override
    public ArrayList<E>  findLowerThan(Comparator<E> cmp, E element) {
         ArrayList<E> resultado= new ArrayList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==-1){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }

    @Override
    public ArrayList<E> findGreaterThan(Comparator<E> cmp, E element) {
    ArrayList<E> resultado= new ArrayList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==1){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }

    @Override
   public ArrayList<E> findBetween(Comparator<E> cmp, E o1, E o2) {
        ArrayList<E> resultado= new ArrayList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if((cmp.compare(e,o1)==1)&&(cmp.compare(e,o2)==-1)){
                    
                    resultado.addLast(e);
                }
                    }
                
        return resultado;
            }
   
   public ListIterator<E> listIterator(int index) {
        if (index < 0 || index > efectivo)
            throw new IndexOutOfBoundsException("Index: "+index);
        return new ListI(index);
    }
    public ListIterator<E> listIterator() {
        return new ListI(0);
    }
    
   public class MyIterator implements Iterator<E>{
        int puntero;       // indice del siguiente elemento
        int lastR = -1; // valor del puntero al estar al final
        int capEsperada = capacity;
        @Override
        public boolean hasNext() {
            return puntero != size();
        }
       @Override
        public E next() {
            try {
                int i = puntero;
                E next = (E) get(i);
                lastR = i;
                puntero = i + 1;
                return next;
            } catch (IndexOutOfBoundsException e) {
                throw new NoSuchElementException();
            }
        }
        @Override
        public void remove() {
            if (lastR < 0)
                throw new IllegalStateException();

            try {
                ArrayList.this.remove(lastR);
                puntero= lastR;
                lastR = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
        }
   
      private class ListI extends MyIterator implements ListIterator<E> {
        ListI(int index) {
            super();
            puntero = index;
        }

        @Override
        public boolean hasPrevious() {
            return puntero != 0;
        }

        @Override
        public int nextIndex() {
            return puntero;
        }

        @Override
        public int previousIndex() {
            return puntero - 1;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E previous() {
            int i = puntero - 1;
            if (i < 0)
                throw new NoSuchElementException();
            Object[] elementData = ArrayList.this.array;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            puntero = i;
            return (E) elementData[lastR = i];
        }
         
   
        @Override
        public void set(E e) {
            if (lastR < 0)
                throw new IllegalStateException();
            try {
                ArrayList.this.set(lastR, e);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(E e) {
            try {
                int i = puntero;
                ArrayList.this.add(i,e);
                puntero = i + 1;
                lastR = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }


       
}}

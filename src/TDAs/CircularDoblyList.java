/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import List.List;
import Model.Foto;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Evelyn
 * @param <E>
 */
public class CircularDoblyList<E> implements List<E>, Iterable<E>{
    
    private Nodo<E> last;
    private Nodo<E>pointer;
    protected boolean found;         // true if element found, else false
    private Comparator<E> comp;


        public CircularDoblyList(){
            pointer = last = null;

        }
        public CircularDoblyList(Comparator<E> comp){
            pointer = last = null;
            this.comp= comp;

        }

                public Nodo<E> getNodeLast() {
        return last;
    }

                 public void setNodeLast(Nodo<E> last) {
        this.last = last;
    }
    
                public Nodo<E> getNodeFirst() {
        return last.getNext();
    }

                @Override
                public boolean isEmpty(){	
		return (last == null);
	}
	
                @Override
                public int size() {
                  int size = 0;
                  if (isEmpty())
                      return size;
                  else {
                       Nodo currentNode = last.getNext();
                       do {
                           currentNode = currentNode.getNext();
                           size++;
                       } while (currentNode != last.getNext());
                  }    
                  return size;      
              }
   
                public boolean add(E element){
                     	Nodo<E> newest= new Nodo<>(element);   // Reference to the new node being added
	   	
	   	if (isEmpty()){
                                last=newest;
                                last.setNext(newest);
                                last.setPrev(newest);	   	 
	   	 }
	   	 else                      // Adding into a non-empty list
	   	 {
                                Nodo<E> header= last.getNext();
	   		 last.setNext(newest);
	   		 newest.setPrev(last);
	   	     last = newest;
	   	     last.setNext(header);
	   	 }
//	   	 numElements++;
                        return true;
	}
        
                private void addBetween(E e, Nodo<E> predecessor, Nodo<E> successor) { 
                        Nodo<E> newest = new Nodo<>(e, predecessor, successor);
                        predecessor.setNext(newest);
                        successor.setPrev(newest);
                        newest.setPrev(predecessor);
                        newest.setNext(successor);
                        //size++;
                      }
              
                @Override
                  public boolean addFirst(E e) {
                      Nodo<E> newest;
                       if(e == null)  return false;
                       else if(isEmpty()){
                          newest = new Nodo(e);
                          last=newest;
                          last.setNext(newest);
                          last.setPrev(newest);
                      }else{
                          Nodo<E> header= last.getNext();
	          newest = new Nodo(e);
                          newest.setNext(header);
	   		header.setPrev(newest);
	   	    header = newest;
	   	    header.setPrev(last);
	   	    last.setNext(header);
                                     	  
                      }
                       return true;
                    }
                  
                @Override
                 public boolean addLast(E e) {
                        Nodo<E> newest;  
                        if(isEmpty()){
                          newest = new Nodo(e);
                          last=newest;
                          last.setNext(newest);
                          last.setPrev(newest);
                      }else{
                       Nodo<E> header= last.getNext();
	          newest = new Nodo(e);
                         
                        
	   		 last.setNext(newest);
	   		 newest.setPrev(last);
	   	     last = newest;
	   	     last.setNext(header);
   
                      }
                 return true;
                 }

            @Override
            public E getFirst() {
                return last.getNext().getData();
            }

            @Override
            public E getLast() {
                return last.getData();
            }
            
            public void print() {
                                  if (isEmpty()) {
                                    System.out.println("List is empty!");
                                } else {
                                    Nodo<E> temp = last.getNext();
                                    do {
                                        System.out.print(temp.getData() + " ");
                                        temp = temp.getNext();
                                    } while (temp != last.getNext());
                                }

                           }

            public void printReverse() {
                                //Node<E> header=last.getNext();
                                if (isEmpty()) {
                                    System.out.println("List is empty!");
                                } else {
                                    Nodo<E> temp = last;
                                    do {
                                        System.out.print(temp.getData() + " ");
                                        temp = temp.getPrev();
                                    } while (temp != last.getNext());
                                }

                           }


       
            protected void findPosition(int position){
                    int start = 0;
                    pointer = last.getNext();
                    found = false;

                    if ((!isEmpty()) && (position >= 0) && (position <= size())) {
                            do {
                                    // move search to the next node
                                    pointer = pointer.getNext();
                                    start++;

                            } while ((pointer != last.getNext()) && start < position);
                            found = true;
                    }

            }

            @Override
            public boolean insert(int position,E data){
                 Nodo<E> newNode = new Nodo<>(data);
                        Nodo<E> header=last.getNext();
                        if (isEmpty()) {
                                // add element to an empty list
                                     header = newNode;
                             last = newNode;   	    
                             header.setPrev(last);
                             last.setNext(header);
                            return true;
                        }else if(data == null)  { return false;
                        } else if (position <= 0 ) {
                                    return addFirst(data);
                                            
                        } else if (position >= size()) {
                            
                            return addLast(data);
                        } 
                                findPosition(position);
                                newNode.setPrev(pointer.getPrev());
                                                        newNode.setNext(pointer);
                                                        pointer.getPrev().setNext(newNode);
                                                        pointer.setPrev(newNode);
                                                         return true;   
                        
                //	size++;
                         }

	
            protected void find(E target){// Searches list for an occurrence of an element. If successful, sets instance variables
    
      Nodo<E>location = last.getNext();
      found = false;
      if(!isEmpty()){ 	  
    	  do 
          {
            if (location.getData().equals(target))  // if they match
            {
             found = true;
             return;
            }
            else
            {
                location = location.getNext();
            }
          }while(location != last.getNext());
      }
      
    }

             @Override
            public boolean contains (E element){
	// Returns true if this list contains an element e such that 
	// e.equals(element), otherwise returns false.
	
		 find(element);
		 return found;
	}
    
              @Override
            public int indexOf(E element) {
             if(element == null) return -1;
                Nodo<E>location = last.getNext();
                int i= 0;
                if(!isEmpty()){ 	  
                      do {
                            if (location.getData().equals(element)){
                                return i;
                            }
                            else{
                                location = location.getNext();
                                 i++;
                            }
                     }while(location != last.getNext());  
                      
                }
        
                 return -1;
    }

	
             @Override
            public boolean remove(E e){
                Nodo<E> header=last.getNext();
                   Nodo<E> cn = header;
                    while(cn.getData()!= e){
                            cn = cn.getNext();
                    }

                if(cn.getData() != null){
                        if(cn.getPrev() == null){
                                header = cn.getNext();
                                if(!isEmpty()){
                                        header.setPrev(null);
                                }
                        }else if(cn.getNext() == null){
                                last = cn.getPrev();
                                if(!isEmpty()){
                                        last.setNext(null);
                                }
                        }else{
                                cn.getPrev().setNext(cn.getNext());
                                cn.getNext().setPrev(cn.getPrev());
                        }
                }
                
                return cn.getData() != null;
        }
                @Override	
	 public boolean removeFirst(){
		 Nodo<E> header=last.getNext();
	        if(!isEmpty()){  
	        	
	        	if(header.getNext() == header){  //if the first element is the only element in the list,	        		             //it empties the list
	        		header = null;
	        		last = null;
	        	}else{			//removes the first element
	              header = header.getNext();
	              header.setPrev(last);
	              last.setNext(header);              
	        	}
	        }else return false;
                return true;
	//        size--;
	 }
         
	@Override	 
	 public boolean removeLast(){
	 
		 Nodo<E> header=last.getNext();
	     if(!isEmpty()){
	    	 
	    	 if(header.getNext() == header){ //if the last element is the only element in the list,
	                          //it empties the list      	
	    		 header = null;
	    		 last = null;
	         }else{				//removes the last element
	            last = last.getPrev();
	            last.setNext(header);
	            header.setPrev(last); 
	         }
	     }else return false;
	   //  size--;
           return true;
	    }
	@Override	  
        //cambios en remove
	 public E remove(int position){
		      
                   //Node<E> header=last.getNext();
		 if(position <= 0){	
                                        E header=last.getNext().getData();
                                        removeFirst();
                                                 return header;
		 }else if(position >= size() - 1){ //remove last element
	                        E lastt=last.getData();
                                        removeLast();
                                                    return lastt;
			 
		 }
			    findPosition(position);
                                                    E pt=pointer.getData();
                                                    pointer.getPrev().setNext(pointer.getNext());
                                                    pointer.getNext().setPrev(pointer.getPrev());
		 
	//	 size--;
                            return pt;
	 }
    
          @Override
          public E set(int index, E element){
              if (element == null || index < 0 || index >= size()){
                    return null;
                 }
              if(index==size()-1){
                    E temp=last.getData();
                    last.setData(element);
                    return temp;
              }
            int j = 0;      
            for(Nodo<E> i = last.getNext(); i != last; i = i.getNext()){
                if(j == index){
                    E tmp = i.getData();
                    i.setData(element);
                return tmp;
            }
        }
        return null;
          }
         

        @Override	 
       public E get(int index) {
        if(index < 0 || index >= size())
            return null;
        int j = 0;
        int cant=size();
        if(index==cant-1) return last.getData();
        for(Nodo<E> i = last.getNext(); i != last; i = i.getNext()){
            if(j == index)
                return i.getData();
            j++;
        }
        return null;
    }
       
     @Override
    public CircularDoblyList<E> findAll(Comparator<E> cmp, E element) {
        CircularDoblyList result=new CircularDoblyList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==0){
                result.addLast(e);
            }
            
        }
        return result;
    }

      @Override
    public CircularDoblyList  findLowerThan(Comparator<E> cmp, E element) {
        CircularDoblyList resultado= new CircularDoblyList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==-1){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }

    @Override
    public CircularDoblyList findGreaterThan(Comparator<E> cmp, E element) {
    CircularDoblyList resultado= new CircularDoblyList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==1){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }

    @Override
    public CircularDoblyList findBetween(Comparator<E> cmp, E o1, E o2) {
        CircularDoblyList resultado= new CircularDoblyList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if((cmp.compare(e,o1)==1)&&(cmp.compare(e,o2)==-1)){
                    
                    resultado.addLast(e);
                }
                    }
                
        return resultado;
            }    
    
     public Iterator<E> iterator( ) { 
       return new CircularDoblyList.LinkedCIterator(); // create a new instance of the inner class
   }

    @Override
    public String toString() {
        
    	String item = "[ ";
        
    		Nodo<E> current = last.getNext();
 
    		if(!isEmpty()){
    			
    			do{
        			item += current.getData() + " ";
        			current = current.getNext();
        		}while(current != last.getNext());
    			
    		}
    		item += "]";
        return item;
    }

    

     class LinkedCIterator implements Iterator<E> {
       private Nodo<E> pointerN;
       private Nodo<E> lastN=null;
       private int index;

        public LinkedCIterator() {
       pointerN=last.getNext();
       index=0;
        }
    
       
       
       @Override
        public boolean hasNext() {
        //int num= size();
        return (index<size());

        }
       @Override
        public E next() {
             if (pointerN == null) throw new NoSuchElementException("No hay nada");

            lastN = pointerN; 
            pointerN = pointerN.getNext();
            index++;
           
           return  lastN.getData();
        }



    
}
}




package TDAs;

import List.List;
import java.util.Comparator;
import java.util.Iterator;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;


/**
 *
 * @author Administrador
 * @param <E>
 */
public class LinkedList <E> implements List<E>, Iterable<E>{
    private Nodo<E> first, last;
    private int efectivo;
    private Comparator comp;
    
    public LinkedList(){
        first = last = null;
        efectivo = 0;
    }
      public LinkedList(Comparator<E> comp){
        first = last = null;
        efectivo = 0;
        this.comp=comp;
    }
      
      public boolean sortedInsert(E element){
        if(element == null){
            return false;
        }else if(isEmpty() || comp.compare(element,first.getData())>0){
            System.out.println(element +"elemento");    
            return addFirst(element);
                
        }else if(comp.compare(element,last.getData())<0)
                return addLast(element);
        for(Nodo<E> p = first.getNext(); p!=null ; p=p.getNext())
        {
            Nodo<E> nodo = new Nodo<>(element);
            if(comp.compare(element,p.getData())>=0)
            {
                Nodo<E> siguiente = p.getNext();
                nodo.setNext(siguiente);
                p.setNext(nodo);
                efectivo ++;
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean addFirst(E element) {
        Nodo<E> nodo = new Nodo<>(element);
        if(element == null)
            return false;
        else if(isEmpty())
            first = last = nodo;
        else{
            nodo.setNext(first);
            first = nodo;
        }
        efectivo++;
        return true;
    }

    @Override
    public boolean addLast(E element) {
        Nodo<E> nodo = new Nodo<>(element);
        if(element == null)
            return false;
        else if(isEmpty())
            first = last = nodo;
        else{
            last.setNext(nodo);
            last = nodo;
        }
        
        efectivo ++;
        return true;
    }

    @Override
    public E getFirst() {
        return first.getData();
    }

    @Override
    public E getLast() {
        return last.getData();
    }

    @Override
    public boolean removeFirst() {
        if(isEmpty())
            return false;
        else if(first == last)
            first = last = null;
        else{
            Nodo<E> tmp = first;
            first = first .getNext();
            tmp.setNext(null);
        }
        efectivo--;
        return true;
    }
    
    
    @Override
    public boolean removeLast(){
        if(this.isEmpty()) //si está vacío no se saca el nodo
            return false;
        else if(first == last) //si ambos son iguales solo hay un nodo en el arreglo
            first = last= null; //solo ese se remueve
        else{
            
            //Iterar nodos con un while hasta antes del last
            Nodo<E> nodo = first;
            while(nodo.getNext() != last){
                nodo = nodo.getNext();
            }
            
            last = nodo;
            last.setNext(null);
            
            /*
            Si se quiere iterar nodos hasta el final sería
            while(nodo.getNext() != null){     Ya que el último nodo no tiene
                nodo = nodo.getNext();         un nodo siguiente
            }
            */
        }
        return true;
    }
    
    private Nodo<E> getPrevious(Nodo<E> nodo){
        if(nodo == first)
            return null;
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(i.getNext() == nodo)
                return i;
        }
        return null;
    }
    
    @Override
    public boolean isEmpty() {
        return (first == null && last == null);
    }

    @Override
    public boolean contains(E element) {
        if(element == null || isEmpty()){
            return false;
        }
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(i.getData().equals(element))
                return true;
        }
        return false;
    }

    @Override
    public boolean insert(int index, E element) {
        if(element == null || index < 0 || index >= efectivo) {
            return false;
        }else if(index == 0){
            addFirst(element);
            efectivo ++;
            return true;
        }else if( index == efectivo-1){
            addLast(element);
            efectivo ++;
            return true;
        }
        
        int i = 0;
        Nodo<E> nodo = new Nodo<>(element);
        for(Nodo<E> j = first; j != null; j = j.getNext()){
            if(index -1  == i){
                nodo.setNext(j.getNext());
                j.setNext(nodo);
                efectivo++;
                return true;
            }
            i++;
        }
        return false;
    }

    @Override
    public E get(int index) {
        if(index < 0 || index >= efectivo)
            return null;
        int j = 0;
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(j == index)
                return i.getData();
            j++;
        }
        return null;
    }

    @Override
    public int indexOf(E element) {
        if(element == null)
            return -1;
        
        int j = 0;
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(i.getData().equals(element))
                return j;
            j++;
        }
        return -1;
    }

    @Override
    public E remove(int index) {
        if(index < 0 || index >= efectivo){
            return null;
        }else if(index == 0){
            E tmp = getFirst();
            removeFirst();
            return tmp;
       }else if( index == efectivo - 1){
           E tmp = getLast();
           removeLast();
           return tmp;
        }
        
        Nodo<E> j = first.getNext();
        for (int i = 1; i != index-1; i++){
            j = j.getNext();
        }
        
        Nodo<E> tmp = j.getNext();
        j.setNext(tmp.getNext());
        tmp.setNext(null);
        efectivo--;
        return tmp.getData();
        
        /**
        int j = 0;
        // Se empieza con el segundo porque ya validamos que hacer con el first
        for(Node<E> i = first.getNext(); i != null; i = i.getNext()){
            if(j == index){
                Node<E> previo = getPrevious(i);
                previo.setNext(i.getNext());
                i.setNext(null);
                efectivo--;
                return i.getData();
            }
            j++;
        }
        return null;**/
    }

    @Override
    public boolean remove(E element) {
        if(element == null) {
            return false;
        }else if(first.getData().equals(element)){
            removeFirst();
            return true;
        }else if(last.getData().equals(element)){
           removeLast();
           return true;
        }
        
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(i.getData().equals(element)){
                Nodo<E> previo = getPrevious(i);
                previo.setNext(i.getNext());
                i.setNext(null);
                efectivo--;
                return true;
            }
        }
        return false;
    }

    @Override
    public E set(int index, E element) {
        if (element == null || index < 0 || index >= efectivo){
            return null;
        }
        
        int j = 0;
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(j == index){
                E tmp = i.getData();
                i.setData(element);
                return tmp;
            }
        }
        return null;
    }

    @Override
    public int size() {
        return efectivo;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null || ! (o instanceof LinkedList))
            return false;
        LinkedList<E> lista = (LinkedList<E>) o;
        
        if(efectivo != lista.efectivo)
            return false;
        
        Nodo<E> nodo = lista.first;
        
        for(Nodo<E> i = first; i !=null; i = i.getNext()){
            if(!nodo.getData().equals(i.getData()))
                return false;
            
            nodo = nodo.getNext();
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.first);
        hash = 83 * hash + this.efectivo;
        return hash;
    }
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        if(isEmpty())
            return "[]";
        s.append("[");
        
        for(Nodo<E> i = first; i != null; i = i.getNext()){
            if(i != last)
                s.append(i.getData() + ",");
            else
                s.append(i.getData() + "]");
        }
        return s.toString();
    }
    
    public Nodo getFirstNode () {
        return first;
    }
    
    public Nodo getLastNode () {
        return last;
    }
    
     public Iterator<E> iterator( ) { 
       return new LinkedList.LinkedIterator(); // create a new instance of the inner class
   }
 @Override
    public LinkedList<E> findAll(Comparator<E> cmp, E element) {
        LinkedList result=new LinkedList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==0){
                result.addLast(e);
            }
            
        }
        return result;
    }

      @Override
    public LinkedList<E>  findLowerThan(Comparator<E> cmp, E element) {
        LinkedList<E> resultado= new LinkedList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==-1){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }

    @Override
    public LinkedList<E> findGreaterThan(Comparator<E> cmp, E element) {
    LinkedList<E> resultado= new LinkedList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if(cmp.compare(e,element)==1){
                resultado.addLast(e);
            }           
        }
        return resultado;
    }

    @Override
    public LinkedList<E> findBetween(Comparator<E> cmp, E o1, E o2) {
        LinkedList<E> resultado= new LinkedList();
        Iterator<E> it= this.iterator();
        while(it.hasNext()){
            E e=it.next();
            if((cmp.compare(e,o1)==1)&&(cmp.compare(e,o2)==-1)){
                    
                    resultado.addLast(e);
                }
                    }
                
        return resultado;
            }    
     
   private class LinkedIterator implements Iterator<E> {
       private Nodo<E> puntero = first; 
       private Nodo<E> lastN = null;
    
       
       
       @Override
        public boolean hasNext() {
           
        return (puntero != null);

        }
       @Override
        public E next() {
            if (puntero == null) throw new NoSuchElementException("No hay nada");
            lastN = puntero; 
            puntero = puntero.getNext();
           return  lastN.getData();
        }

      
    }
   
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

/**
 *
 * @author Administrador
 */
public class Nodo<E> {
    private E data;
    private Nodo<E> prev; 
    private Nodo<E> next;

    public Nodo(E data, Nodo<E> next) {
        this.data = data;
        this.next = next;
    }

    public Nodo(Nodo<E> prev, Nodo<E> next) {
        this.prev = prev;
        this.next = next;
    }

    public Nodo(E data, Nodo<E> prev, Nodo<E> next) {
        this.data = data;
        this.prev = prev;
        this.next = next;
    }
    

    public Nodo<E> getPrev() {
        return prev;
    }

    public void setPrev(Nodo<E> prev) {
        this.prev = prev;
    }
    
    
    
    public Nodo(E data){
        this.data = data;
        this.next = null;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Nodo<E> getNext() {
        return next;
    }

    public void setNext(Nodo<E> next) {
        this.next = next;
    } 
}

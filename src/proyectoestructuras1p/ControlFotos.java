/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import TDAs.ArrayList;
import Model.Foto;
import Model.Persona;
import TDAs.CircularDoblyList;
import TDAs.LinkedList;
import TDAs.Nodo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javafx.scene.Scene;

/**
 *
 * @author Evelyn
 */
public class ControlFotos {
    
    HashMap<String,Foto> mapFoto;
    private CircularDoblyList<Foto> fotos; 

    public ControlFotos(CircularDoblyList<Foto> fotos) {
        this.mapFoto = new HashMap<>();
        this.fotos = fotos;
    }
    
   public ControlFotos() {
        this.mapFoto = new HashMap<>();
        fotos= new CircularDoblyList();

              
    }

   
   public boolean agregarFoto(String URL, String id,  String nombreP, String fecha, String lugar, LinkedList<String> nombreA,
              ArrayList<String> reacciones, ArrayList<String> comentarios, String descripcion, ArrayList<String> hashtags,
              ArrayList<String> personas, String camara) {
       int posFoto = buscarFoto( id );
        boolean add = false;
        Date fechaF= ParseFecha(fecha); 
        if( posFoto == -1 ){
            Foto nuevoF= new Foto(URL,id,nombreP,fechaF, lugar, nombreA,reacciones,comentarios, descripcion, hashtags,personas,camara);
            Foto nuevoFoto= new Foto(URL,nombreP,fechaF, lugar, nombreA,reacciones,comentarios, descripcion, hashtags,
            personas,  camara);
            fotos.add(nuevoF);
            mapFoto.put(id,nuevoF );
            
            add = true;
            System.out.println("Se agrego foto"+ lugar);
        }
        
        //verificarCondiciones( );
        
        return true;
    }

   
    
    public int buscarFoto( String id ){
               int posicion = -1;
               boolean posFinal = false;
           if(!fotos.isEmpty()){    
               Iterator it2= fotos.iterator();
               int indice=0;
             while(it2.hasNext()){
                          Foto fotoI=(Foto) it2.next();
                         String nombre= fotoI.getId();
                              if(nombre.equals(id)){
                               posicion = indice;
                               posFinal = true;
                     }
            indice++;
             }
           }else return posicion;
       return posicion;
    }
    
     public CircularDoblyList<Foto> getFotos() {
            return fotos;
    }

    public void setFotos(CircularDoblyList<Foto> fotos) {
            this.fotos = fotos;
    }
    
    
    //Todos los fotos de un usuario
   
   /*
    public static void main(String[] args) {
        ControlFotos c= new ControlFotos();
               CircularDoblyList<Foto> list= c.getFotos();
               HashMap<String, CircularDoblyList<Foto>> m=c.mapFoto; 
               ArrayList<String> nombreA= new ArrayList();
               nombreA.addFirst("a");
               nombreA.addFirst("v");
               ArrayList<String> reacciones= new ArrayList();
               reacciones.addFirst("a1");
               reacciones.addFirst("v2");
               ArrayList<String> comentarios= new ArrayList();
               comentarios.addFirst("a3");
               comentarios.addFirst("v3");
               ArrayList<String> hashtags= new ArrayList();
               hashtags.addFirst("a");
               hashtags.addFirst("v");
               ArrayList<String> personas= new ArrayList();
               personas.addFirst("a");
               personas.addFirst("v");
                                  
          */
               
               
    //           list.add(new Foto("URL",  "id",  "nombreP", "fecha", " lugar",nombreA,reacciones, comentarios,"descripcion",hashtags, personas, "camara"));
       //        list.add(new Foto("URL3",  "i3d",  "n3ombreP", "fecha", " lugar",nombreA,reacciones, comentarios,"descripcion",hashtags, personas, "camara"));
          //     list.add(new Foto("UR4L",  "i4d",  "nombreP", "fecha", " lugar",nombreA,reacciones, comentarios,"descripcion",hashtags, personas, "cam4ara"));
             //  System.out.println(c.getFotos().get(0).getId());
               
         //      int n=c.buscarFoto("i3d");
              // System.out.println(n);
              
               
               /*
               int posicion = -1;
               boolean posFinal = false;
               Iterator it2= list.iterator();
               int indice=0;
             while(it2.hasNext()){
                 
            //Foto foto = c.getFotos().get(indice);
             // String idFoto = foto.getId();
           
            Foto fotoI=(Foto) it2.next();
           String nombre= fotoI.getId();
          
           if(nombre.equals("i4d")){
                posicion = indice;
                posFinal = true;
            }
            indice++;
        }
        System.out.println(posicion);
    }*/
               
               
    
     public Date ParseFecha(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        return fechaDate;
    }
     
     public void OrdenarPorFecha(CircularDoblyList<Foto> fotos){
         
        }
     /**
      * 
      * @param foto
      * @param num 1 si es a la derecha, -1 si es a la izquierda
      * @return 
      */
     public Foto cambiofoto(Foto foto, int num){
                                 System.out.println("id de foto en control"+foto.getId());  

         if((num==1) &&(foto.getId().equals(fotos.getLast().getId()))) return fotos.getFirst();
        // if((num==-1) &&(foto.equals(fotos.getFirst()))) return fotos.getLast();
         Foto nuevaFoto= foto;
         for(Nodo <Foto> i=fotos.getNodeFirst(); i!=fotos.getNodeLast();i=i.getNext()){
             if(foto.getId().equals(i.getData().getId())){
                 nuevaFoto=i.getNext().getData();
                 return nuevaFoto;
             }
         }
        return null;
         }   
     
     
     
     }
            
        
    
   
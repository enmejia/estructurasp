/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import TDAs.ArrayList;
import Model.Album;
import Model.Comentario;
import Model.Foto;
import Model.Hashtag;
import Model.Persona;
import Model.Reaccion;
import TDAs.LinkedList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 *
 * @author Evelyn
 */
public class Main extends Application {
        //    PantallaInicio inicio;
        //    PantallaFotos fotosP;
    ControlPersona cpersona;
    ControlFotos cfoto;
    ControlAlbum calbum;
    ControlPersonaE cpersonaE;
    ControlHashTag chash;
    
     private Stage stage;
    public static final String ARCHIVO_PERSONAS = "src/ArchivosTexto/personas.txt";
    public static final String ARCHIVO_FOTOS = "src/ArchivosTexto/fotos.txt";
    public static final String ARCHIVO_ALBUMS = "src/ArchivosTexto/album.txt";
    public static final String ARCHIVO_COMENTARIOS = "src/ArchivosTexto/comentarios.txt";
    public static final String ARCHIVO_REACCIONES = "src/ArchivosTexto/reaccion.txt";
    public static final String ARCHIVO_PERSONASE = "src/ArchivosTexto/personasE.txt";
    public static final String ARCHIVO_HASHTAG = "src/ArchivosTexto/hashT.txt";


    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
 
   
    @Override
    public void start(Stage primaryStage) {
        stage=primaryStage;
        
        cpersona = new ControlPersona( );
        cfoto = new ControlFotos( );
        calbum= new ControlAlbum();
            cpersonaE= new ControlPersonaE();
            chash= new ControlHashTag();
        

        cargarArchivo( ARCHIVO_PERSONAS);
        
       cargarArchivoFoto( ARCHIVO_FOTOS);
       cargarArchivoAlbum( ARCHIVO_ALBUMS);
       cargarArchivoPersonasE( ARCHIVO_PERSONASE);
       cargarArchivoHashT(ARCHIVO_HASHTAG);

        

        PantallaInicio inicio=new PantallaInicio();  
    
        
        mostrarLogin(inicio, primaryStage);
        
         
       inicio.cambioPantallaFotos(primaryStage,cfoto, cpersona,calbum,this);

        primaryStage.setTitle("Hello World!");
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
               
        launch(args);
    }
    
    public void mostrarLogin(PantallaInicio inicio, Stage primaryStage){
        VBox root1 = inicio.getPrincipal();
        Scene scene = new Scene(root1, 300, 180);
        primaryStage.setScene(scene);
        
    }
       
  public void cargarArchivo( String archivo ){
      
        try{
            FileInputStream lecturaArch = new FileInputStream( new File( archivo ) );
            Properties propiedades = new Properties( );
            propiedades.load(lecturaArch );

            String iD,clave,nombreP,apellido,imagenP,sup,correo,contrasena;
            String ctePersona="persona";
            clave = "total.personas";
            sup = propiedades.getProperty( clave );
            int totalPersonas = Integer.parseInt(sup );

            for( int i = 1; i <= totalPersonas; i++ ){
                
                clave = ctePersona + i + ".id";
                iD = propiedades.getProperty(clave );
//                System.out.println("se agrego id,"+ iD);
                
                clave = ctePersona + i + ".nombre";
                nombreP = propiedades.getProperty(clave );
   //             System.out.println("se agrego nombre,"+ nombreP);
                
                clave = ctePersona + i + ".apellido";
                apellido = propiedades.getProperty(clave );
      //          System.out.println("se agrego ap,"+ apellido);
                
                clave = ctePersona + i + ".imagen";
                imagenP = propiedades.getProperty(clave );
         //       System.out.println("se agrego imagenP,"+ imagenP);
                
                clave = ctePersona + i + ".correo";
                correo = propiedades.getProperty(clave );
            //    System.out.println("se agrego co,"+ correo);
                
                
                clave = ctePersona + i + ".contrasena";
                contrasena = propiedades.getProperty(clave );
               // System.out.println("se agrego contra,"+contrasena);
                
              
                if( iD != null && nombreP != null && apellido != null && correo != null &&contrasena != null ){
                  cpersona.agregarPersona(iD,nombreP, apellido, imagenP, correo, contrasena);
                  
                         HashMap<String,Persona> m=cpersona.mapa;
                     m.entrySet().forEach((entry) -> {
                        System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue());
                         }); 
                System.out.println("se agrego persona total: "+ iD);
             
      
            }
                lecturaArch.close( );
            }
          }
        catch( FileNotFoundException e )
        {
         error("No se encontró el archivo ");
        }
        catch( IOException e )
        {
        error("Error al serializar archivo");

        }
    }
  
  public void cargarArchivoFoto( String archivo ){
      
        try{
            FileInputStream lecturaArch = new FileInputStream( new File( archivo ) );
            Properties propiedades = new Properties( );
            propiedades.load(lecturaArch );
           
            String clave,sup,url,id,idP,nombreP,fecha,lugar,nombreA,reaccion,comentario,descripcion,persona,hashtag,camara;
            
            String cteFoto="foto";
            clave = "total.fotos";
            sup = propiedades.getProperty( clave );
            int totalPersonas = Integer.parseInt(sup );

            for( int i = 1; i <= totalPersonas; i++ ){
                
                clave = cteFoto + i + ".id";
                id = propiedades.getProperty(clave );
                
                clave = cteFoto + i + ".idPersona";
                nombreP = propiedades.getProperty(clave );
                
                clave = cteFoto + i + ".url";
                url = propiedades.getProperty(clave );
                
                clave = cteFoto + i + ".fecha";
                fecha = propiedades.getProperty(clave );
                
                clave = cteFoto + i + ".lugar";
                lugar = propiedades.getProperty(clave );
               
                
                clave = cteFoto + i + ".idAlbum";
                LinkedList<String> album= new LinkedList();
                nombreA = propiedades.getProperty(clave );
                String[] partsN = nombreA.split(",");               
                for (String partsN1 : partsN) {
                    album.addLast(partsN1);
                }

              
                clave = cteFoto + i + ".idReaccion";
                reaccion = propiedades.getProperty(clave );
                ArrayList<String> reacciones= new ArrayList();
                String[] partsR = reaccion.split(",");
                
                for (String partsR1 : partsR) {
                    reacciones.addLast(partsR1);
                }
                
                
                clave = cteFoto + i + ".idComentario";
                comentario = propiedades.getProperty(clave );
                 ArrayList<String> comentarios= new ArrayList();
                String[] partsC = comentario.split(",");
                
                for (String partsC1 : partsC) {
                    comentarios.addLast(partsC1);
                }
                
                
                
                clave = cteFoto + i + ".idHashtags";
                hashtag = propiedades.getProperty(clave );
                 ArrayList<String> hashtags= new ArrayList();
           
                String[] partsH = hashtag.split(",");
                
                for (String partsH1 : partsH) {
                    hashtags.addLast(partsH1);
                }
                
              
                clave = cteFoto + i + ".descripcion";
                descripcion = propiedades.getProperty(clave );
                
                
                clave = cteFoto + i + ".idPersonas";
                persona = propiedades.getProperty(clave );
                 ArrayList<String> personas= new ArrayList();
           
                 String[] partsP = persona.split(",");
                
                for (String partsP1 : partsP) {
                    personas.addLast(partsP1);
                }
                
                
                clave = cteFoto + i + ".camara";
                camara = propiedades.getProperty(clave );
               ///if(cfoto.getFotos().isEmpty())    cfoto.getFotos().add(new Foto(url,id,nombreP,fecha,lugar,album,reacciones,comentarios,descripcion,hashtags,personas,camara));

               //else 
                   if( id != null && nombreP != null){
                    
                    cfoto.agregarFoto(url,id,nombreP,fecha,lugar,album,reacciones,comentarios,descripcion,hashtags,personas,camara);
         
                   }
                lecturaArch.close( );
            }
          }
        catch( FileNotFoundException e )
        {
         error("No se encontró el archivo ");
        }
        catch( IOException e )
        {
        error("Error al serializar archivo");

        }
    }
  
  public void cargarArchivoAlbum( String archivo ){
      
        try{
            FileInputStream lecturaArch = new FileInputStream( new File( archivo ) );
            Properties propiedades = new Properties( );
            propiedades.load(lecturaArch );

            
            String clave, sup,idAlbum, nombreAlbum, comentario, idPersona;
           int nElementos;
            String cteAlbum="album";
            clave = "total.albumes";
            sup = propiedades.getProperty( clave );
            int totalPersonas = Integer.parseInt(sup );

            for( int i = 1; i <= totalPersonas; i++ ){
                
                clave = cteAlbum + i + ".id";
                idAlbum = propiedades.getProperty(clave );

                clave = cteAlbum + i + ".nombre";
                nombreAlbum = propiedades.getProperty(clave );

                clave = cteAlbum + i + ".idfotos";
                LinkedList<String> idfotos= new LinkedList();
                String ids = propiedades.getProperty(clave );
                String[] partsN = ids.split(",");               
                for (String partsN1 : partsN) {
                    idfotos.addLast(partsN1);
                }

                clave = cteAlbum + i + ".cantidad";
                sup = propiedades.getProperty(clave );
                nElementos = Integer.parseInt(sup );          
                
                
                clave = cteAlbum + i + ".comentario";
                comentario= propiedades.getProperty(clave );
                
              
                clave = cteAlbum + i + ".idPersona";
                idPersona= propiedades.getProperty(clave );
                
              
                if( idAlbum != null && nombreAlbum != null ){
                  calbum.agregarAlbum(idAlbum,nombreAlbum, idfotos, nElementos, comentario, idPersona);
               // System.out.println("se agrego album: "+ idAlbum);     
               
               
            }
                lecturaArch.close( );
            }
          }
        catch( FileNotFoundException e )
        {
         error("No se encontró el archivo ");
        }
        catch( IOException e )
        {
        error("Error al serializar archivo");

        }
    }
 
    public void cargarArchivoPersonasE(String archivo){
        
            try{
            FileInputStream lecturaArch = new FileInputStream( new File( archivo ) );
            Properties propiedades = new Properties( );
            propiedades.load(lecturaArch );
            
            String clave, sup,idPersonaE, nombrePE, apellidoE, idPersona;
            String ctePersonaE="persona";
            clave = "total.personas";
            sup = propiedades.getProperty( clave );
            int totalPersonas = Integer.parseInt(sup );

            for( int i = 1; i <= totalPersonas; i++ ){
                
                clave = ctePersonaE + i + ".id";
                idPersonaE = propiedades.getProperty(clave );
                System.out.println(idPersonaE);
                
                clave = ctePersonaE + i + ".nombre";
                nombrePE = propiedades.getProperty(clave );
                 System.out.println(nombrePE);
                
                 clave = ctePersonaE + i + ".apellido";
                apellidoE = propiedades.getProperty(clave );

                clave = ctePersonaE + i + ".idPersona";
                idPersona= propiedades.getProperty(clave );
              
                if( idPersonaE != null && nombrePE != null && idPersona!=null ){
                  cpersonaE.agregarPersonaE(idPersonaE,nombrePE,apellidoE, idPersona);
                System.out.println("se agrego personaE: "+ idPersonaE);     
               
               
            }
                lecturaArch.close( );
            }
          }
        catch( FileNotFoundException e )
        {
         error("No se encontró el archivo ");
        }
        catch( IOException e )
        {
        error("Error al serializar archivo");

        }
            }
    
        public void cargarArchivoHashT(String archivo){
        
            try{
            FileInputStream lecturaArch = new FileInputStream( new File( archivo ) );
            Properties propiedades = new Properties( );
            propiedades.load(lecturaArch );
            
            String clave, sup,hashs,idHash, contenido,idPersona;
            String idHashT="hash";
            clave = "total.hash";
            sup = propiedades.getProperty( clave );
            int totalPersonas = Integer.parseInt(sup );

            for( int i = 1; i <= totalPersonas; i++ ){
                
                clave = idHashT + i + ".id";
                idHash = propiedades.getProperty(clave );
                System.out.println(idHash);
                
                clave = idHashT + i + ".contenido";
                contenido= propiedades.getProperty(clave );
                
                clave = idHashT + i + ".idPersona";
                idPersona= propiedades.getProperty(clave );
              
                if( idHash != null && contenido != null && idPersona!=null ){
                  chash.agregarHashT(idHash,contenido,idPersona);
                //System.out.println("se agrego hash: "+ idHash);     
               
               
            }
                lecturaArch.close( );
            }
          }
        catch( FileNotFoundException e )
        {
         error("No se encontró el archivo ");
        }
        catch( IOException e )
        {
        error("Error al serializar archivo");

        }
            }

    
  /*private void actualizarArchivo( String archivo) throws IOException
    {
        int tamañoAnt=cpersona.getPersonas().size()-1;
        int tamaño=cpersona.getPersonas().size();                     
          try{
                FileOutputStream fos= new FileOutputStream( new File( archivo),true );
                Properties prop = new Properties( );
                Persona persona = ( Persona )cpersona.getPersonas().get( tamañoAnt );
                String   iD,dato,nombre, apellido, imagen, correo, contraseña;
        
                dato = "total.personas";
                prop.setProperty(dato, String.valueOf(tamaño));
    
                iD = "persona" + (tamaño) + ".id";
                prop.setProperty(iD, persona.getNombre());
                
                nombre = "persona" + (tamaño) + ".nombre";
                prop.setProperty(nombre, persona.getNombreP());
                
                apellido = "persona" + (tamaño) + ".apellido";
                prop.setProperty(apellido, persona.getApellido());
                
                imagen = "persona" + (tamaño) + ".imagen";
                prop.setProperty(imagen, persona.getImagenP());
               
                correo = "persona" + (tamaño) + ".correo";
                prop.setProperty(correo,persona.getCorreo());
               
                contraseña = "persona" + (tamaño) + ".contraseña";
                prop.setProperty(contraseña,persona.getContraseña());
                          
                
                prop.store(fos, null);
                fos.close();
                
        }     
        catch( FileNotFoundException e )
        {
            error("No se encontro archivo");      
        }
        catch( IOException e )
        {
             error("Error al serializar");
        }
    }
  */
        public void error(String mensaje){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setHeaderText(null);

        alert.setContentText(mensaje);
        alert.showAndWait();
    
    }

    }           
                   
    
    
    
    
    
    


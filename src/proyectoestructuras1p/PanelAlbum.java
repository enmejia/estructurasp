/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import TDAs.ArrayList;
import Model.Album;
import Model.Foto;
import TDAs.CircularDoblyList;
import TDAs.LinkedList;
import TDAs.Nodo;
import java.util.Comparator;
import java.util.HashMap;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Evelyn
 */
public class PanelAlbum {
        GridPane gridAlbum;
   //     GridPane gridFotos;
       private LinkedList<Album> albumsUsuario;
       private     Label LabelImagenExp;
       private  Foto fotoEscogida;
       private   ScrollPane scroll;
       private  HashMap<String, CircularDoblyList<Foto>> mapIdAlbumFotos;
       private  BorderPane root;
       private  Button izq,der,info;
       private  ImageView imageViewExpandido;
      
      
         
       public GridPane getGridAlbum() {
        return gridAlbum;
    }

    public void setGridAlbum(GridPane gridAlbum) {
        this.gridAlbum = gridAlbum;
    }

    public LinkedList<Album> getAlbumsUsuario() {
        return albumsUsuario;
    }

    public void setAlbumsUsuario(LinkedList<Album> albumsUsuario) {
        this.albumsUsuario = albumsUsuario;
    }
        

    public PanelAlbum(GridPane gridAlbum, StackPane paneAlbum) {
        this.gridAlbum = gridAlbum;
    }
        public PanelAlbum(PanelFoto pantallafotos, ControlAlbum calbum){
        panelAlbum(pantallafotos,calbum);    
        }
        
        public ScrollPane panelAlbum(PanelFoto pantallafotos, ControlAlbum calbum){
    //    StackPane panePrincipal=pantallafotos.getStack();
         
        scroll= new ScrollPane();
        gridAlbum = new GridPane();
        scroll.setContent(gridAlbum);
//        Button botonAlbum=pantallafotos.getPestañaAlbum();
   //     botonAlbum.setOnAction(e->{
           //panePrincipal.getChildren().add(scroll);
         
                    gridAlbum.setPadding(new Insets(10,10,10,10));
                    gridAlbum.setHgap(10);
                    gridAlbum.setVgap(10);
                    int cols=2, colCnt = 0, rowCnt = 0;
                    
                    
        LinkedList <Album> listaAlbumT= calbum.getIds();
        String idU=pantallafotos.getIdU();
       
             
         Comparator<Album> cmpAlbumIdPersona = new Comparator<Album>() {
          
             @Override
            public int compare(Album o1, Album o2) {
                       if (o1.getIdPersona().equals(o2.getIdPersona())) {
            return 0;
            } else {
            return 1;
            }
        } 
        };
    
       

        Album AlbumxPersona= new Album();
        AlbumxPersona.setIdPersona(idU);
        albumsUsuario = listaAlbumT.findAll(cmpAlbumIdPersona, AlbumxPersona);
        //
        
        LinkedList<String> idAlbumsUsuario= new LinkedList<>(); 
        for(Album a:albumsUsuario){
            idAlbumsUsuario.addLast(a.getIdAlbum());
        }
                
        mapIdAlbumFotos=new HashMap<>();
        CircularDoblyList<Foto> fotosUsuario=pantallafotos.getFotosUsuario();
        //LinkedList<CircularDoblyList<Foto>> la=new LinkedList<>();        
        for(String i:idAlbumsUsuario){
               CircularDoblyList<Foto> fotosxAlbum= new CircularDoblyList<>();
               for(Foto fot:fotosUsuario){
                if(fot.getNombreA().contains(i)){
                    fotosxAlbum.addLast(fot);
                }
            }//la.addLast(fotosxAlbum);
               mapIdAlbumFotos.put(i, fotosxAlbum);
        }

        for(Album album:albumsUsuario){
             Label l= new Label();
             l.setMinSize(200, 200);
             Image imag=new Image("data/compu.jpg"); //se tenia que crear uno nuevo 
             ImageView image=new ImageView(imag);
             image.setFitHeight(200);
             image.setFitWidth(200);
             l.setGraphic(image);
            gridAlbum.add(l, colCnt, rowCnt);
            colCnt++;
            if (colCnt>cols) {
                rowCnt++;
                colCnt=0;
            }
            l.setOnMouseClicked(e->{
                        abrirAlbum(album);
             });
             
         
        }
            return scroll;
        
        }
        
        public void abrirAlbum(Album album){
                VBox vbox=new VBox();
                Button regresar= new Button("atras");
                regresar.setMinSize(100, 50);
                GridPane gridFotos=new GridPane();
                gridFotos.setPadding(new Insets(10,10,10,10));
                gridFotos.setHgap(10);
                gridFotos.setVgap(10);
                int col=2, colCn = 0, rowCn = 0;
                      
                vbox.getChildren().addAll(regresar,gridFotos);
                
                for (HashMap.Entry<String, CircularDoblyList<Foto>>entry : mapIdAlbumFotos.entrySet()) {
                  String clave=entry.getKey();
                  if(album.getIdAlbum().equals(clave)){
                     for(Foto f:entry.getValue()){
                         
                                    Label labelFoto=new Label();
                                    labelFoto.setMinSize(200, 200);
                                    Image img=new Image((f.getURL()));  
                                    ImageView imagen=new ImageView(img);
                                    imagen.setFitHeight(200);
                                    imagen.setFitWidth(200);
                                    labelFoto.setGraphic(imagen);
                                   gridFotos.add(labelFoto, colCn, rowCn);
                                   colCn++;
                                    if (colCn>col) {
                                    rowCn++;
                                    colCn=0;
                                    }
                                    labelFoto.setOnMouseClicked(e -> {
                                                HBox central=new HBox();      
                                                root= new BorderPane();
                                                izq= new Button("<");
                                                izq.setAlignment(Pos.CENTER);
                                                der= new Button(">");
                                                der.setAlignment(Pos.CENTER);
                                                LabelImagenExp= new Label();

                                               central.getChildren().addAll(izq,LabelImagenExp,der);                 
                                               central.setAlignment(Pos.CENTER);
                                               root.setCenter(central);

                                                fotoEscogida= new Foto(); 
                                                fotoEscogida=f;
                                                Stage nuevaP= new Stage();
                                                HBox topFotoE=new HBox();
                                                Label labelTituloFoto=new Label("Titulo");
                                                labelTituloFoto.setMinSize(550, 35);
                                                labelTituloFoto.setAlignment(Pos.CENTER);
                                                Button botonInfoFoto=new Button();
                                                botonInfoFoto.setMinSize(100, 35);
                                                topFotoE.getChildren().addAll(labelTituloFoto, botonInfoFoto);
                                                root.setTop(topFotoE);

                                                Image imag= new Image(fotoEscogida.getURL());
                                                imageViewExpandido=new ImageView();
                                                imageViewExpandido.setImage(imag);
                                                imageViewExpandido.setFitHeight(550);
                                                imageViewExpandido.setFitWidth(550);
                                                LabelImagenExp.setGraphic(imageViewExpandido);




                                                  Scene nuevaS= new Scene(root, 780, 600);
                                                 nuevaP.setScene(nuevaS);
                                                 nuevaP.setTitle("foto");
                                                  nuevaP.show();    


                                               botonInfoFoto.setOnAction(b->{
                                                   accionBotonInfo();
                                               });


                                              der.setOnMouseClicked(d->{
                                                  accionBotonDerecho(entry.getValue());
                                              });

                                              izq.setOnMouseClicked(i->{
                                                  accionBotonIzquierdo(entry.getValue());
                                              });
                                            });
                        

                     }
                   }

                }
                        
        scroll.setContent(vbox);
        regresar.setOnAction(j->{
            scroll.setContent(gridAlbum);
        });
        }
              public void accionBotonIzquierdo(CircularDoblyList<Foto> fotosUsuario){
               if(fotoEscogida.getId().compareTo(fotosUsuario.getFirst().getId())==0){
                                Foto fotoant=fotosUsuario.getLast();      
                                String nuevaI=fotosUsuario.getLast().getURL();
                                LabelImagenExp.setGraphic(new ImageView(new Image(nuevaI,550,550,false,false)));
                               fotoEscogida=fotoant;
                        }else{    
                                Foto fotoanterior = null;                 
                            for(Nodo<Foto> i=fotosUsuario.getNodeLast(); i!=fotosUsuario.getNodeFirst();i=i.getPrev()){
                                    if(fotoEscogida.getId().compareTo(i.getData().getId())==0){
                                            fotoanterior=i.getPrev().getData();
                                            String nuevaI=fotoanterior.getURL();
                                            LabelImagenExp.setGraphic(new ImageView(new Image(nuevaI,550,550,false,false)));
                                     }
                            }fotoEscogida=fotoanterior;
                        }
                
   } 
    public void accionBotonDerecho(CircularDoblyList<Foto> fotosUsuario){
                if(fotoEscogida.getId().compareTo(fotosUsuario.getLast().getId())==0){
                                Foto fotosig=fotosUsuario.getFirst();      
                                String nuevaI=fotosUsuario.getFirst().getURL();
                                 ImageView imgv=new ImageView(new Image(nuevaI,550,550,false,false));
                                LabelImagenExp.setGraphic(imgv);
                               fotoEscogida=fotosig;
                }else{    
                                Foto fotosiguiente = null;                 
                            for(Nodo<Foto> i=fotosUsuario.getNodeFirst(); i!=fotosUsuario.getNodeLast();i=i.getNext()){
                                    if(fotoEscogida.getId().compareTo(i.getData().getId())==0){
                                            fotosiguiente=i.getNext().getData();
                                            String nuevaI=fotosiguiente.getURL();
                                            ImageView imgview=new ImageView(new Image(nuevaI,550,550,false,false));
                                            LabelImagenExp.setGraphic(imgview);
                                           
                                     }
                            }fotoEscogida=fotosiguiente;
                        }
                
    }
    public void accionBotonInfo(){
                        VBox vboxInfo= new VBox();
                        vboxInfo.setMinSize(200, 600);
                        GridPane grip= new GridPane();
                        grip.setHgap(8);
                        grip.setVgap(8);
                        Label lab= new Label("nuevo panel");
                         grip.add(lab,0,0);
                        
                        Label tituloId= new Label("Id");
                        grip.add(tituloId,0,1);
                        Label labelId= new Label();
                        labelId.setText(fotoEscogida.getId());
                          grip.add(labelId,0,2);
                          
                         Label tituloFecha= new Label("Fecha");
                        grip.add(tituloFecha,0,3);
                         Label labelFecha= new Label();
                         labelFecha.setText(fotoEscogida.getFecha().toString());
                         grip.add(labelFecha,0,4); 
                        
                         Label tituloLugar= new Label("Lugar");
                         grip.add(tituloLugar,0,5);
                         Label labelLugar= new Label();
                         labelLugar.setText(fotoEscogida.getLugar());
                        grip.add(labelLugar,0,6);
                        
                         Label tituloAlbum= new Label("Album");
                        grip.add(tituloAlbum,0,7);
                         Label labelAlbum= new Label();
                        labelAlbum.setText(fotoEscogida.getNombreA().toString());
                        grip.add(labelAlbum,0,8);
                        
                         Label tituloPersona= new Label("Personas");
                        grip.add(tituloPersona,0,9);
                        
                         Label labelPersona= new Label();
                        labelPersona.setText(fotoEscogida.getPersonas().toString());
                        grip.add(labelPersona,0,10);
                        
                        Label tituloDescrip= new Label("Descripcion");
                        grip.add(tituloDescrip,0,11);
                        Label  labelDescrip= new Label();
                        
                        labelDescrip.setText(fotoEscogida.getDescripcion());
                        grip.add(labelDescrip,0,12);
                        
                         Label tituloHash= new Label("HashTags");
                        grip.add(tituloHash,0,13);
                         Label labelHashs= new Label();
                        labelHashs.setText(fotoEscogida.getHashtags().toString());
                        grip.add(labelHashs,0,14);
                        
                         Label tituloCam= new Label("Camara");
                        grip.add(tituloCam,0,15);
                         Label labelCamara= new Label();
                        labelCamara.setText(fotoEscogida.getCamara());
                        grip.add(labelCamara,0,16);
                        
                        vboxInfo.getChildren().add(grip);
                        root.setRight(vboxInfo);
    }
      }
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import TDAs.ArrayList;
import Model.Album;
import Model.Foto;
import TDAs.CircularDoblyList;
import TDAs.LinkedList;
import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Evelyn
 */
public class PanelCrear {
    private Button botonFoto;
    private Button botonAlbum;
    private Button botonPersona;
    private BorderPane border;
    
     public PanelCrear(Main principal, ControlAlbum calbum, PanelAlbum palbum){
        panelCrear(principal, calbum, palbum);    
        }

    public HBox panelCrear(Main principal, ControlAlbum calbum, PanelAlbum palbum){
        HBox hbox=new HBox(100);
        botonFoto= new Button("Añadir foto");
        botonFoto.setMinSize(200, 200);
        botonAlbum= new Button("Añadir Album");
        botonAlbum.setMinSize(200, 200);
        botonPersona= new Button("Añadir Persona");
        botonPersona.setMinSize(200, 200);
        hbox.getChildren().addAll(botonFoto,botonAlbum,botonPersona);
        hbox.setAlignment(Pos.CENTER);
        hbox.setMinSize(500, 400);
        hbox.setBackground(new Background(new BackgroundFill(Color.GAINSBORO, CornerRadii.EMPTY, Insets.EMPTY)));
        
        botonFoto.setOnAction(e->{
            FileChooser fc = new FileChooser();
            configuringFileChooser(fc);           
            File resultado = fc.showOpenDialog(principal.getStage());
            if( resultado != null ){ 
                GridPane grip=new GridPane();
            grip.setHgap(8);
            grip.setVgap(8);
            Button btnAgregar=new Button("Agregar");
            HBox hboxFoto=new HBox(100);
            hboxFoto.setAlignment(Pos.CENTER);
            hboxFoto.setPadding(new Insets(10,10,10,10));
            VBox vboxD= new VBox();
            VBox vboxI= new VBox();
            hboxFoto.getChildren().addAll(vboxD,vboxI);
            Label labelFoto= new Label();
            labelFoto.setMinSize(450, 450);
             Label labelTitulo= new Label();
             labelTitulo.setMinSize(400, 100);

            ImageView fotoView= new ImageView();
            TextField textTitulo= new TextField();
            VBox vboxInfo= new VBox();
    
                        TextArea  txtDescrip= new TextArea("Añade un descripcion");
                        txtDescrip.setMinSize(100, 100);

                                             
            Label tituloFecha= new Label("Fecha");
            grip.add(tituloFecha,0,3);
            DatePicker txtFecha= new DatePicker();
            grip.add(txtFecha,0,4); 
                        
                         Label tituloLugar= new Label("Lugar");
                         grip.add(tituloLugar,0,5);
                         TextField txtlLugar= new TextField();
                        grip.add(txtlLugar,0,6);
                        
                        LinkedList<Album>pal=palbum.getAlbumsUsuario();
                        HashMap<String,Album> mapA=calbum.mapAlbum;
                        ArrayList<String> nombres = new ArrayList();

                        
                        for(Album pa:pal){
                        for (Map.Entry<String,Album> entry :mapA.entrySet()) {
                             if(entry.getKey().equals(pa.getIdAlbum())) nombres.add(entry.getValue().getNombreAlbum());
                                 }
                        }
                      
                        
                        Label tituloAlbum= new Label("Album");
                        grip.add(tituloAlbum,0,7);
                        ComboBox txtAlbum= new ComboBox();
                        for(String a:nombres){
                        txtAlbum.getItems().add(a);    
                        }
                        
                        grip.add(txtAlbum,0,8);
                        
                        
                        Label tituloPersona= new Label("Personas");
                        grip.add(tituloPersona,0,9);
                        TextField txtlPersona= new TextField();
                        grip.add(txtlPersona,0,10);
                    
                         Label tituloHash= new Label("HashTags");
                        grip.add(tituloHash,0,11);
                        TextField txtHashs= new TextField();
                        grip.add(txtHashs,0,12);
                        
                         Label tituloCam= new Label("Camara");
                        grip.add(tituloCam,0,13);
                         TextField txtCamara= new TextField();
                        grip.add(txtCamara,0,14);
                        
                        grip.add(btnAgregar, 0, 16);
                        
                        vboxD.getChildren().addAll(labelTitulo,textTitulo,labelFoto,txtDescrip);
                        vboxI.getChildren().addAll(grip);
                         btnAgregar.setOnAction(b-> {
                             
           // String fecha = txtFecha.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")); ;
            //String horaN = (String)hora.getValue()+":"+(String)minuto.getValue() ;
            
                         });
            
            
            
            
                String texto=resultado.toURI().toString();
                 Stage nuevaP= new Stage();
                Scene nuevaS= new Scene(hboxFoto, 780, 600);
                   nuevaP.setScene(nuevaS);
                   nuevaP.setTitle("foto");
                    nuevaP.show();    
                    
          //	txtImagen.setText(texto);        
            
            }
          
            
            /*this.nombreP = nombreP;
        this.fecha = fecha;
        this.lugar = lugar;
        this.nombreA = nombreA;
        this.reacciones = reacciones;
        this.comentarios = comentarios;
        this.descripcion = descripcion;
        this.hashtags = hashtags;
        this.personas = personas;
        this.camara = camara;*/
                    
        });
        
        
        return hbox;
        
    }

      private void configuringFileChooser(FileChooser file) {
      // Set title for FileChooser
      file.setTitle("Seleccione la imagen");
 
      // Set Initial Directory
      File workingDirectory = new File("src/data");
      file.setInitialDirectory(workingDirectory);
      file.getExtensionFilters().addAll(
              new FileChooser.ExtensionFilter("JPG", "*.jpg"), 
              new FileChooser.ExtensionFilter("PNG", "*.png"));
  }






}

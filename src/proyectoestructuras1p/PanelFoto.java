/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import TDAs.ArrayList;
import Model.Album;
import Model.Foto;
import Model.Persona;
import TDAs.CircularDoblyList;
import TDAs.LinkedList;
import TDAs.Nodo;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 *
 * @author Evelyn
 */
public class PanelFoto {
  
   private GridPane gridFotos;
    private ImageView imageView;
    private Foto imgURL;
    private Image img;
    private  Button izq, der;
    private   ImageView imageViewExpandido;
    private Label LabelImagenExp;
    private Image imagenExp;
    private Foto fotoEscogida; /*fotosiguiente;/*,fotoanterior;*/
   /* private Label lab, tituloId, labelId, tituloFecha,labelFecha,tituloLugar,labelLugar,tituloAlbum,labelAlbum,tituloPersona,
                                labelPersona, tituloDescrip,labelDescrip,tituloHash,labelHashs,tituloCam,labelCamara;
    */private String idU;
    private  BorderPane root;
    
    private CircularDoblyList<Foto> fotosUsuario; 

    public CircularDoblyList<Foto> getFotosUsuario() {
        return fotosUsuario;
    }

    public GridPane getGridFotos() {
        return gridFotos;
    }

    public void setGridFotos(GridPane gridFotos) {
        this.gridFotos = gridFotos;
    }

    public void setFotosUsuario(CircularDoblyList<Foto> fotosUsuario) {
        this.fotosUsuario = fotosUsuario;
    }

    public String getIdU() {
        return idU;
    }

    public void setIdU(String idU) {
        this.idU = idU;
    }

    public Foto getImgURL() {
        return imgURL;
    }

    public void setImgURL(Foto imgURL) {
        this.imgURL = imgURL;
    }


    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
   
     public PanelFoto(PantallaInicio inicioP, ControlPersona cpersona,ControlFotos cfoto){
        crearPantallaFotos(inicioP,cpersona,cfoto);
    }    

    
     public GridPane crearPantallaFotos(PantallaInicio inicioP, ControlPersona cpersona,ControlFotos cFoto){

        gridFotos = new GridPane();
        gridFotos.setPadding(new Insets(10,10,10,10));
        gridFotos.setHgap(10);
        gridFotos.setVgap(10);
//       int pos= cpersona.buscarPersonaCorreo(inicioP.getUsuario().getText());
        int cols=2, colCnt = 0, rowCnt = 0;
   
        HashMap<String,Foto> mapFotos=cFoto.mapFoto;
        
     
        idU=cpersona.getIdUsario();
        CircularDoblyList <Foto> listafotosT= cFoto.getFotos();
         System.out.println("tamaño listaFotosT"+listafotosT);
             
         Comparator<Foto> cmpFotoIdPersona = new Comparator<Foto>() {
          
             @Override
            public int compare(Foto o1, Foto o2) {
                       if (o1.getNombreP().equals(o2.getNombreP())) {
            return 0;
            } else {
            return 1;
            }
        } 
        };
    
         
         
               System.out.println("");
   Foto fotoxPersona= new Foto();
   fotoxPersona.setNombreP(idU);
               
        fotosUsuario = listafotosT.findAll(cmpFotoIdPersona, fotoxPersona);
                System.out.println("tamañoLista"+fotosUsuario.size());
               
//CircularDoblyList<Foto> usuarioFechas= new CircularDoblyList<Foto>(Date::CompareTo); 
                 
              HashSet<Date> setFechas= new HashSet();
              /*        
         Comparator<Foto> cmp = new Comparator<Foto>() {
             @Override
             public int compare(Foto o1, Foto o2) {
                if(o1.getFecha().after(o2.getFecha())) return 1;
                else if(o1.getFecha().before(o2.getFecha())) return 1;
                else return 0;
             }
             
};        */
                    
             Map<Date, CircularDoblyList<String>> hash2=new LinkedHashMap();
                 
               System.out.println("fotoxUsario"+fotosUsuario.size());
                for(Foto f:fotosUsuario){
                        setFechas.add(f.getFecha());  
                       }  
                
               LinkedList<Date> fotoFecha= new LinkedList<>(Date::compareTo);

                   for(Date f:setFechas){ 
                        fotoFecha.sortedInsert(f);
                    }
                    
                   
                   
                    for(Date s:fotoFecha){
                        hash2.put(s, new CircularDoblyList<>());
                    }
                
                 for(Date fecha:fotoFecha){
                     for(Foto foto:fotosUsuario){
                          if(foto.getFecha().equals(fecha))  hash2.get(fecha).add(foto.getURL());
                     }
      
                 }
                   
                   
/*                   
           System.out.println("mapa");
              for (Map.Entry<Date, CircularDoblyList<String>>  entry :hash2.entrySet()) {
                    System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue());

              }*/

            for(Foto foto:fotosUsuario){
             Label l= new Label();
             l.setMinSize(200, 200);
             Image imagP=new Image((foto.getURL())); //se tenia que crear uno nuevo 
             ImageView image=new ImageView(imagP);
             image.setFitHeight(200);
             image.setFitWidth(200);
             l.setGraphic(image);
            gridFotos.add(l, colCnt, rowCnt);
            colCnt++;
            if (colCnt>cols) {
                rowCnt++;
                colCnt=0;
            }
         
              l.setOnMouseClicked(e -> {     
                     HBox central=new HBox();      
                     root= new BorderPane();
                     izq= new Button("<");
                     izq.setAlignment(Pos.CENTER);
                     der= new Button(">");
                     der.setAlignment(Pos.CENTER);
                     LabelImagenExp= new Label();

                    central.getChildren().addAll(izq,LabelImagenExp,der);                 
                    central.setAlignment(Pos.CENTER);
                    root.setCenter(central);
                  
                  fotoEscogida= new Foto(); 
                  fotoEscogida=foto;
                  Stage nuevaP= new Stage();
                  HBox topFotoE=new HBox();
                  Label labelTituloFoto=new Label("Titulo");
                  labelTituloFoto.setMinSize(550, 35);
                  labelTituloFoto.setAlignment(Pos.CENTER);
                  Button botonInfoFoto=new Button();
                  botonInfoFoto.setMinSize(100, 35);
                  topFotoE.getChildren().addAll(labelTituloFoto, botonInfoFoto);
                  root.setTop(topFotoE);
                  
                  Image imagen= new Image(fotoEscogida.getURL());
                  imageViewExpandido=new ImageView();
                  imageViewExpandido.setImage(imagen);
                  imageViewExpandido.setFitHeight(550);
                  imageViewExpandido.setFitWidth(550);
                  LabelImagenExp.setGraphic(imageViewExpandido);
                    
                    
                    
                 
                    Scene nuevaS= new Scene(root, 780, 600);
                   nuevaP.setScene(nuevaS);
                   nuevaP.setTitle("foto");
                    nuevaP.show();    
                    
                     
                 botonInfoFoto.setOnAction(b->{
                     accionBotonInfo();
                 });
                    
                    
                der.setOnMouseClicked(f->{
                    accionBotonDerecho();
                });
                
                izq.setOnMouseClicked(f->{
                    accionBotonIzquierdo();
                });
              });    
        }
            
     
       return gridFotos;
    }
    private void configuringFileChooser(FileChooser file) {
      // Set title for FileChooser
      file.setTitle("Seleccione la imagen");
 
      // Set Initial Directory
      File workingDirectory = new File("src/data");
      file.setInitialDirectory(workingDirectory);
      file.getExtensionFilters().addAll(
              new FileChooser.ExtensionFilter("JPG", "*.jpg"), 
              new FileChooser.ExtensionFilter("PNG", "*.png"));
  }
    
   public void accionBotonIzquierdo(){
               if(fotoEscogida.getId().compareTo(fotosUsuario.getFirst().getId())==0){
                                System.out.println("fotoEscogida"+fotoEscogida);
                                Foto fotoant=fotosUsuario.getLast();      
                                String nuevaI=fotosUsuario.getLast().getURL();
                                LabelImagenExp.setGraphic(new ImageView(new Image(nuevaI,550,550,false,false)));
                               fotoEscogida=fotoant;
                        }else{    
                                Foto fotoanterior = null;                 
                            for(Nodo<Foto> i=fotosUsuario.getNodeLast(); i!=fotosUsuario.getNodeFirst();i=i.getPrev()){
                                    if(fotoEscogida.getId().compareTo(i.getData().getId())==0){
                                            fotoanterior=i.getPrev().getData();
                                            String nuevaI=fotoanterior.getURL();
                                            LabelImagenExp.setGraphic(new ImageView(new Image(nuevaI,550,550,false,false)));
                                            System.out.println("la foto siguiente es"+fotoanterior.getId());
                                     }
                            }fotoEscogida=fotoanterior;
                        }
                
   } 
    public void accionBotonDerecho(){
                if(fotoEscogida.getId().compareTo(fotosUsuario.getLast().getId())==0){
                                System.out.println("fotoEscogida"+fotoEscogida);
                                Foto fotosig=fotosUsuario.getFirst();      
                                String nuevaI=fotosUsuario.getFirst().getURL();
                                 ImageView imgv=new ImageView(new Image(nuevaI,550,550,false,false));
                                LabelImagenExp.setGraphic(imgv);
                               fotoEscogida=fotosig;
                               
                        }else{    
                                Foto fotosiguiente = null;                 
                            for(Nodo<Foto> i=fotosUsuario.getNodeFirst(); i!=fotosUsuario.getNodeLast();i=i.getNext()){
                                    if(fotoEscogida.getId().compareTo(i.getData().getId())==0){
                                            fotosiguiente=i.getNext().getData();
                                            String nuevaI=fotosiguiente.getURL();
                                            ImageView imgview=new ImageView(new Image(nuevaI,550,550,false,false));
                                            LabelImagenExp.setGraphic(imgview);
                                            System.out.println("la foto siguiente es"+fotosiguiente.getId());
                                            
                                     }
                            }fotoEscogida=fotosiguiente;
                        }
                
    }
    public void accionBotonInfo(){
                        VBox vboxInfo= new VBox();
                        vboxInfo.setMinSize(200, 600);
                        GridPane grip= new GridPane();
                        grip.setHgap(8);
                        grip.setVgap(8);
                        Label lab= new Label("nuevo panel");
                         grip.add(lab,0,0);
                        
                        Label tituloId= new Label("Id");
                        grip.add(tituloId,0,1);
                        Label labelId= new Label();
                        labelId.setText(fotoEscogida.getId());
                          grip.add(labelId,0,2);
                                                 
                         Label tituloFecha= new Label("Fecha");
                        grip.add(tituloFecha,0,3);
                         Label labelFecha= new Label();
                         labelFecha.setText(fotoEscogida.getFecha().toString());
                         grip.add(labelFecha,0,4); 
                        
                         Label tituloLugar= new Label("Lugar");
                         grip.add(tituloLugar,0,5);
                         Label labelLugar= new Label();
                         labelLugar.setText(fotoEscogida.getLugar());
                        grip.add(labelLugar,0,6);
                        
                         Label tituloAlbum= new Label("Album");
                        grip.add(tituloAlbum,0,7);
                         Label labelAlbum= new Label();
                        labelAlbum.setText(fotoEscogida.getNombreA().toString());
                        grip.add(labelAlbum,0,8);
                        
                         Label tituloPersona= new Label("Personas");
                        grip.add(tituloPersona,0,9);
                        
                         Label labelPersona= new Label();
                        labelPersona.setText(fotoEscogida.getPersonas().toString());
                        grip.add(labelPersona,0,10);
                        
                        Label tituloDescrip= new Label("Descripcion");
                        grip.add(tituloDescrip,0,11);
                        Label  labelDescrip= new Label();
                        
                        labelDescrip.setText(fotoEscogida.getDescripcion());
                        grip.add(labelDescrip,0,12);
                        
                         Label tituloHash= new Label("HashTags");
                        grip.add(tituloHash,0,13);
                         Label labelHashs= new Label();
                        labelHashs.setText(fotoEscogida.getHashtags().toString());
                        grip.add(labelHashs,0,14);
                        
                         Label tituloCam= new Label("Camara");
                        grip.add(tituloCam,0,15);
                         Label labelCamara= new Label();
                        labelCamara.setText(fotoEscogida.getCamara());
                        grip.add(labelCamara,0,16);
                        
                        vboxInfo.getChildren().add(grip);
                        root.setRight(vboxInfo);
    }
}

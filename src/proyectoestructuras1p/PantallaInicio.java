/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import Model.Persona;
import TDAs.LinkedList;
import java.util.HashMap;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 *
 * @author Evelyn
 */
public class PantallaInicio {
    private Button inicioSesion;
    private Label textUsuario;
    private Label textContrasena;
    private TextField usuario;
    private PasswordField contrasena;
    private String nombreU;
    private Label titulo;
    private VBox principal; 
    private PanelFoto fotosP;

    public PanelFoto getFotosP() {
        return fotosP;
    }

    public void setFotosP(PanelFoto fotosP) {
        this.fotosP = fotosP;
    }

    
    public TextField getUsuario() {
        return usuario;
    }

    public void setUsuario(TextField usuario) {
        this.usuario = usuario;
    }

    public String getNombreU() {
        return nombreU;
    }

    public void setNombreU(String nombreU) {
        this.nombreU = nombreU;
    }
    
    
    public PantallaInicio(){
         contenidoInicio();
    }
    
    public PasswordField getContrasena() {
        return contrasena;
    }

    public Button getInicioSesion() {
        return inicioSesion;
    }
    

    public VBox getPrincipal() {
        return principal;
    }

    public void setPrincipal(VBox principal) {
        this.principal = principal;
    }
    
    public void contenidoInicio(){
           principal= new VBox(20);
           //VBox.setMargin(titulo, new Insets(10,5,10,5));
           titulo= new Label("GALLERY");
           
           titulo.setPrefSize(300, 30);  

           titulo.setTextAlignment(TextAlignment.JUSTIFY);
           titulo.setAlignment(Pos.CENTER);
           principal.getChildren().add(titulo);
           GridPane grid= new GridPane();
           grid.setPadding(new Insets(10));
           grid.setHgap(25);
           grid.setVgap(15);
           grid.setAlignment(Pos.CENTER);
           textUsuario=new Label("Usuario: ");
           textContrasena=new Label("Contraseña: ");
           usuario=new TextField();
           contrasena= new PasswordField();
           grid.add(textUsuario, 0, 0);
           grid.add(textContrasena,0, 1);
           grid.add(usuario, 1, 0);
           grid.add(contrasena, 1, 1);
           principal.getChildren().add(grid);
           inicioSesion= new Button("Entrar");
           inicioSesion.setAlignment(Pos.CENTER);
           inicioSesion.setPrefSize(50, 30);  
           principal.setAlignment(Pos.CENTER);
           principal.getChildren().add(inicioSesion);
           
    }
      public boolean iniciando(ControlPersona persona){
        //String idUsuario;
         // System.out.println("metodo iniciando"+pos);
            HashMap<String,Persona> mapa=persona.mapa;
           if(persona.verificacionCorreo(usuario.getText())==true){
               String idUsuario=persona.buscarIdCorreo(usuario.getText());
               System.out.println("el id del user es"+idUsuario);
               Persona datosPersona=mapa.get(idUsuario);
               System.out.println(datosPersona.toString());
               String contra=datosPersona.getContraseña();
               if(contra.equals(contrasena.getText())){
                        nombreU= mapa.get(idUsuario).getNombreP();
                        persona.setIdUsario(idUsuario);

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Mensaje del sistema");
                        alert.setHeaderText(null);
                        alert.setHeaderText("Proyecto 1er Parcial");
                        alert.setContentText("Inicio de sesion correcto");

                        alert.showAndWait();
                        ///
                        System.out.println("metodo iniciando"+usuario.getText());
                        ///
                    }return true;
           }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Mensaje del sistema");
            alert.setHeaderText("Proyecto");
            alert.setContentText("Inicio de sesion incorrecto");

            alert.showAndWait();
            usuario.setText("");
            contrasena.setText("");
            return false;
        
               }
        }   
      public void cambioPantallaFotos(Stage primaryStage,ControlFotos cfoto,ControlPersona cpersona,ControlAlbum calbum, Main main){
                         
            inicioSesion.setOnAction((ActionEvent e) -> {
            if(iniciando(cpersona)){
                PanelFoto panelFoto= new PanelFoto(this,cpersona,cfoto);
                PanelAlbum panelAlbum= new PanelAlbum(panelFoto,calbum);
                PanelCrear panelcrear=new PanelCrear(main,calbum,panelAlbum);
                PantallaPrincipal pprincipal=new PantallaPrincipal(this, cpersona,panelFoto,panelAlbum,calbum,panelcrear, main);
                BorderPane root=pprincipal.getBorder();
               
                Scene  sceneFotos = new Scene(root, 880, 600);
                primaryStage.setScene(sceneFotos);
            }else{System.out.println("Error al cambiar la pantalla a fotos");}      
            });
                    }
                    
         // LinkedList<Persona> per= persona.getPersonas();
         /*
            if(usuario.getText().equals(per.get(pos).getCorreo()) && contrasena.getText().equals(per.get(pos).getContraseña())){
                nombreU=per.get(pos).getNombreP();
                String idU=per.get(pos).getiD();
                persona.setIdUsario(idU);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Mensaje del sistema");
            alert.setHeaderText(null);
            alert.setHeaderText("Proyecto 1er Parcial");
            alert.setContentText("Inicio de sesion correcto");

            alert.showAndWait();
            ///
            System.out.println("metodo iniciando"+usuario.getText());
            ///
            return true;
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Mensaje del sistema");
            alert.setHeaderText("Proyecto");
            alert.setContentText("Inicio de sesion incorrecto");

            alert.showAndWait();
            usuario.setText("");
            contrasena.setText("");
            return false;
        }}*/
    /*
      public void cambioPantallaFotos(Stage primaryStage,ControlFotos cfoto,ControlPersona cpersona,ControlAlbum calbum){
                         
            inicioSesion.setOnAction((ActionEvent e) -> {
            if(iniciando(cpersona)){
                PanelFoto panelFoto= new PanelFoto(this,cpersona,cfoto);
                PanelAlbum panelAlbum= new PanelAlbum(panelFoto,calbum);
                PantallaPrincipal pprincipal=new PantallaPrincipal(this, cpersona,panelFoto,panelAlbum,calbum);
                BorderPane root=pprincipal.getBorder();
                //PantallaFotos fotoP= new PantallaFotos(this,linkedListFoto,persona,calbum); //no se cambiaba el nombre porque la creacion de la clase estaba fuera.
             //   BorderPane root1 = fotoP.getBorder(); 
                //PantallaAlbum palbum=new PantallaAlbum(fotoP,calbum);          
                
                //System.out.println("inicio sesion"+inicio.getUsuario().getText());  
                
                Scene  sceneFotos = new Scene(root, 880, 600);
                primaryStage.setScene(sceneFotos);
             //aqui si coge el nombre
            }
            
        });

    }*/

    
    }
    
    
    
    
    
    
    
    
    
    
    


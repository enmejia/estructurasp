/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructuras1p;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Evelyn
 */
public class PantallaPrincipal {
    private HBox arriba;
    private BorderPane border;
    private StackPane stack;
    private HBox abajo;
    private Button pestañaFoto;
    private Button pestañaAlbum;
    private Button pestañaCrear;
    private Button pestañaPerfil;
    private Label nombreCuenta;

    public BorderPane getBorder() {
        return border;
    }

    public void setBorder(BorderPane border) {
        this.border = border;
    }

    public Button getPestañaFoto() {
        return pestañaFoto;
    }

    public void setPestañaFoto(Button pestañaFoto) {
        this.pestañaFoto = pestañaFoto;
    }

    public Button getPestañaAlbum() {
        return pestañaAlbum;
    }

    public void setPestañaAlbum(Button pestañaAlbum) {
        this.pestañaAlbum = pestañaAlbum;
    }

    public Button getPestañaCrear() {
        return pestañaCrear;
    }

    public void setPestañaCrear(Button pestañaCrear) {
        this.pestañaCrear = pestañaCrear;
    }

    public Button getPestañaPerfil() {
        return pestañaPerfil;
    }

    public void setPestañaPerfil(Button pestañaPerfil) {
        this.pestañaPerfil = pestañaPerfil;
    }

    public StackPane getStack() {
        return stack;
    }

    public void setStack(StackPane stack) {
        this.stack = stack;
    }
    
    
    public Label getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(Label nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }
    
    public PantallaPrincipal(PantallaInicio inicio,ControlPersona cpersona,PanelFoto fotoP,PanelAlbum palbum, ControlAlbum calbum, PanelCrear pcrear, Main main ){
        crearPPrincipal(inicio,cpersona,fotoP,palbum,calbum,pcrear,main);
    }
    
   public void crearPPrincipal(PantallaInicio inicio,ControlPersona cpersona, PanelFoto fotoP, PanelAlbum palbum, ControlAlbum calbum, PanelCrear pcrear, Main main ){
        border= new BorderPane();
        arriba= new HBox(50);
        arriba.setPadding(new Insets(10,10,10,10));
        TextField buscar= new TextField("Buscar");
        buscar.setPrefSize(410, 40);
        nombreCuenta= new Label();
       
        nombreCuenta.setPrefSize(315, 40);
        
        nombreCuenta.setText(inicio.getUsuario().getText());
        Label fotoC= new Label();
       nombreCuenta.setAlignment(Pos.CENTER_RIGHT);
        fotoC.setMinSize(40, 40);
        fotoC.setAlignment(Pos.CENTER_RIGHT);
      //cambio por mapa  int pos= cpersona.buscarPersonaCorreo(nombreCuenta.getText());
        //cambio  por mapa fotoC.setGraphic(new ImageView(new Image(cpersona.getPersonas().get(pos).getImagenP(),40,40,false,false)));
        arriba.getChildren().addAll(buscar,nombreCuenta,fotoC);
        border.setTop(arriba);
        
        
        stack= new StackPane();
        ScrollPane scroll= new ScrollPane();
        GridPane grid =fotoP.getGridFotos();

        scroll.setContent(grid);
        stack.getChildren().add(scroll);
    
        grid.setPadding(new Insets(10,10,10,10));
        grid.setHgap(10);
        grid.setVgap(10);
        int cols=2, colCnt = 0, rowCnt = 0;
        
                    
        abajo= new HBox();
        int botonX=220;
        int botonY=50;
        pestañaFoto= new Button("pestaña foto");
        pestañaAlbum= new Button("pestaña album");
        pestañaCrear= new Button("pestaña crear");
        pestañaPerfil= new Button("pestaña perfil");
        pestañaPerfil.setPrefSize(botonX, botonY);

       
        pestañaFoto.setPrefSize(botonX, botonY);
        pestañaAlbum.setPrefSize(botonX, botonY);
        pestañaCrear.setPrefSize(botonX, botonY);
        pestañaPerfil.setPrefSize(botonX, botonY);
       
          

        HBox.setMargin(abajo, new Insets(10,10,10,10));
        border.setCenter(stack);
        abajo.getChildren().addAll(pestañaFoto, pestañaAlbum,pestañaCrear, pestañaPerfil);
        border.setBottom(abajo);
        
        pestañaAlbum.setOnAction(g->{
            ScrollPane scroll2=palbum.panelAlbum(fotoP, calbum);
            stack.getChildren().add(scroll2);
                
            });   
        pestañaCrear.setOnAction(g->{
            HBox hb=pcrear.panelCrear(main,calbum,palbum);
            stack.getChildren().add(hb);
                
            });   
        
   } 
}
